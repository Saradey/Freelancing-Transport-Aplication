package transport.frilance.android8.sarad.transportaplication.FragmentRecycleView;


import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import jp.wasabeef.recyclerview.animators.SlideInLeftAnimator;
import transport.frilance.android8.sarad.transportaplication.EntityDataBase.Entity;
import transport.frilance.android8.sarad.transportaplication.FragmentAdd.FragmentAdd;
import transport.frilance.android8.sarad.transportaplication.FragmentMainMenu.FragmentMainMenu;
import transport.frilance.android8.sarad.transportaplication.R;


public class FragmentRecycleView extends Fragment implements intetfaceRecycleViewFragment,
        RecyclerViewClickListener {

    private static final String KEY_INDEX = "key_index";
    private static final String KEY_DOIT = "key_doit";
    private static final String KEY_POSITION = "key_position";
    private static final String TAG = "MyTag";
    private static final int VERTICAL = 1;

    PresenterRecycleViewFragment presenter;
    MyAdapter adapter;

    List<Entity> entityList;

    private int index_list;
    private int position;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_recycle_view, container, false);

        initUIAndArhitectureElem(view);

        return view;
    }


    private void initUIAndArhitectureElem(View view) {
        initIndexList();
        initPresenret();
        initRecycle(view);

        setHasOptionsMenu(true);
    }


    private void initRecycle(View view) {
        entityList = presenter.getList(index_list);
        adapter = new MyAdapter(entityList, this);

        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 1);
        gridLayoutManager.setOrientation(VERTICAL);
        recyclerView.setLayoutManager(gridLayoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                gridLayoutManager.getOrientation());

        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setItemAnimator(new SlideInLeftAnimator());
        recyclerView.setAdapter(adapter);
    }


    private void initPresenret() {
        presenter = new PresenterRecycleViewFragment(this);
    }



    private void initIndexList() {
        Bundle bundle = getArguments();
        index_list = bundle.getInt(KEY_INDEX);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.second_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        int clickedItemPosition = item.getOrder();

        switch (clickedItemPosition) {
            case 1: //обновить
                presenter.goUdpate(2);  //обновить
                break;

            case 2: //удалить
                presenter.delete();
                break;
        }

        return super.onContextItemSelected(item);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.main_add_button:
                presenter.pressOptionsItemSelected(1); //добавить
                return true;

            case R.id.main_back:
                goToTheMainFragment();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void goToTheMainFragment() {
        FragmentMainMenu fragmentAdd = new FragmentMainMenu();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.addToBackStack(null);
        transaction.replace(R.id.field, fragmentAdd);
        transaction.commit();
    }


    @Override
    public void recyclerViewListClicked(View v, int position) {
        this.position = position;
    }


    @Override
    public void goToTheAddFragment(int doit) {
        FragmentAdd fragmentAdd = new FragmentAdd();
        Bundle bundle = new Bundle();
        bundle.putInt(KEY_INDEX, index_list);
        bundle.putInt(KEY_DOIT, doit);
        bundle.putInt(KEY_POSITION, position);
        fragmentAdd.setListEntity(getList());
        fragmentAdd.setArguments(bundle);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.addToBackStack(null);
        transaction.replace(R.id.field, fragmentAdd);
        transaction.commit();
    }


    @Override
    public void delete() {

        switch (index_list) {
            case 0:
                presenter.deleteTown(entityList.get(position).getId());
                break;

            case 1:
                presenter.deleteRoute(entityList.get(position).getId());
                break;

            case 2:
                presenter.deleteDriver(entityList.get(position).getId());
                break;

            case 3:
                presenter.deleteContract(entityList.get(position).getId());
                break;

            case 4:
                presenter.deleteSheduled(entityList.get(position).getId());
                break;

            case 5:
                presenter.deletePointer(entityList.get(position).getId());
                break;

            case 6:
                presenter.deleteCar(entityList.get(position).getId());
                break;

            case 7:
                presenter.deleteInspection(entityList.get(position).getId());
                break;

            case 8:
                presenter.deleteRepair(entityList.get(position).getId());
                break;
        }

    }


    @Override
    public void deleteEntityRecycle() {
        adapter.remove(position);
    }

    public List<Entity> getList() {
        return adapter.getList();
    }


    private void showLog(String show) {
        Log.d(TAG, show);
    }


    private void showToast(String str) {
        Toast toast = Toast.makeText(getActivity().getApplicationContext(),
                str, Toast.LENGTH_SHORT);
        toast.show();
    }


}
