package transport.frilance.android8.sarad.transportaplication.FragmentAdd;


import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import transport.frilance.android8.sarad.transportaplication.EntityDataBase.Entity;
import transport.frilance.android8.sarad.transportaplication.EntityDataBase.EntityCar;
import transport.frilance.android8.sarad.transportaplication.EntityDataBase.EntityContract;
import transport.frilance.android8.sarad.transportaplication.EntityDataBase.EntityDriver;
import transport.frilance.android8.sarad.transportaplication.EntityDataBase.EntityInspection;
import transport.frilance.android8.sarad.transportaplication.EntityDataBase.EntityRepair;
import transport.frilance.android8.sarad.transportaplication.EntityDataBase.EntityRoute;
import transport.frilance.android8.sarad.transportaplication.EntityDataBase.EntityScheduledDeparture;
import transport.frilance.android8.sarad.transportaplication.EntityDataBase.EntityStoppingPoint;
import transport.frilance.android8.sarad.transportaplication.EntityDataBase.EntityTown;
import transport.frilance.android8.sarad.transportaplication.FragmentRecycleView.FragmentRecycleView;
import transport.frilance.android8.sarad.transportaplication.R;


public class FragmentAdd extends Fragment implements InterfaceFragmentAdd {

    private static final String KEY_INDEX = "key_index";
    private static final String KEY_DOIT = "key_doit";
    private static final String TAG = "MyTag";
    private static final String KEY_POSITION = "key_position";
    @BindView(R.id.layout_add)
    LinearLayout layout;
    @BindView(R.id.descript_header)
    TextView header;
    PresenterFragmentAdd presenter;
    private EditText editText1;
    private EditText editText2;
    private EditText editText3;
    private EditText editText4;
    private EditText editText5;

    private Button button_ok;
    private Button button_cancel;

    private int index_list;
    private int doit;
    private int position;
    private List<Entity> record;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_add, container, false);

        ButterKnife.bind(this, view);
        initUIAndArhitectureElem();

        return view;
    }


    private void initUIAndArhitectureElem() {
        loadIndex();
        initPresenter();
        initUIElement();
    }


    private void initPresenter() {
        presenter = new PresenterFragmentAdd(this);
    }


    private void loadIndex() {
        Bundle bundle = getArguments();
        index_list = bundle.getInt(KEY_INDEX);
        doit = bundle.getInt(KEY_DOIT);
        position = bundle.getInt(KEY_POSITION);
    }


    private void initUIElement() {

        switch (index_list) {
            case 0:
                setHeaderText(R.string.list_town_fragment_add, R.string.list_town_fragment_upd);
                initTownElement();
                break;
            case 1:
                setHeaderText(R.string.list_route_fragment_add, R.string.list_route_fragment_upd);
                initRoutElem();
                break;

            case 2:
                setHeaderText(R.string.list_driver_fragment_add, R.string.list_driver_fragment_upd);
                initDriverElem();
                break;

            case 3:
                setHeaderText(R.string.list_contract_fragment_add, R.string.list_contract_fragment_upd);
                initContractElem();
                break;

            case 4:
                setHeaderText(R.string.list_sheduled_fragment_add, R.string.list_sheduled_fragment_upd);
                initShedulerElem();
                break;

            case 5:
                setHeaderText(R.string.list_point_fragment_add, R.string.list_poin_fragment_upd);
                initPoitElem();
                break;

            case 6:
                setHeaderText(R.string.list_car_fragment_add, R.string.list_car_fragment_upd);
                initCarElem();
                break;

            case 7:
                setHeaderText(R.string.list_inspection_fragment_add, R.string.list_inspection_fragment_upd);
                initInspectionElem();
                break;

            case 8:
                setHeaderText(R.string.list_repair_fragment_add, R.string.list_repair_fragment_upd);
                initRepairElem();
                break;
        }
        initButton();

    }

    private void initRoutElem() {
        editText1 = setEditText(R.string.hint_add_Route1);
        editText2 = setEditText(R.string.hint_add_Route2);
        editText3 = setEditText(R.string.hint_add_Route3);
        layout.addView(editText1);
        layout.addView(editText2);
        layout.addView(editText3);
    }


    private void initTownElement() {
        editText1 = setEditText(R.string.hint_add_town);
        layout.addView(editText1);
    }


    private void initRepairElem() {
        editText1 = setEditText(R.string.hint_add_repair1);
        editText2 = setEditText(R.string.hint_add_repair2);
        editText3 = setEditText(R.string.hint_add_repair3);
        layout.addView(editText1);
        layout.addView(editText2);
        layout.addView(editText3);
    }


    private void initInspectionElem() {
        editText1 = setEditText(R.string.hint_add_inspection1);
        editText2 = setEditText(R.string.hint_add_inspection2);
        layout.addView(editText1);
        layout.addView(editText2);
    }


    private void initCarElem() {
        editText1 = setEditText(R.string.hint_add_car1);
        editText2 = setEditText(R.string.hint_add_car2);
        editText3 = setEditText(R.string.hint_add_car3);
        editText4 = setEditText(R.string.hint_add_car4);
        layout.addView(editText1);
        layout.addView(editText2);
        layout.addView(editText3);
        layout.addView(editText4);
    }


    private void initPoitElem() {
        editText1 = setEditText(R.string.hint_add_point1);
        editText2 = setEditText(R.string.hint_add_point2);
        layout.addView(editText1);
        layout.addView(editText2);
    }


    private void initShedulerElem() {
        editText1 = setEditText(R.string.hint_add_sheduled1);
        editText2 = setEditText(R.string.hint_add_sheduled2);
        editText3 = setEditText(R.string.hint_add_sheduled3);
        editText4 = setEditText(R.string.hint_add_sheduled4);
        layout.addView(editText1);
        layout.addView(editText2);
        layout.addView(editText3);
        layout.addView(editText4);
    }


    private void initContractElem() {
        editText1 = setEditText(R.string.hint_add_contracr1);
        editText2 = setEditText(R.string.hint_add_contracr2);
        layout.addView(editText1);
        layout.addView(editText2);
    }


    private void initDriverElem() {
        editText1 = setEditText(R.string.hint_add_driver1);
        editText2 = setEditText(R.string.hint_add_driver2);
        editText3 = setEditText(R.string.hint_add_driver3);
        editText4 = setEditText(R.string.hint_add_driver4);
        editText5 = setEditText(R.string.hint_add_driver5);
        layout.addView(editText1);
        layout.addView(editText2);
        layout.addView(editText3);
        layout.addView(editText4);
        layout.addView(editText5);
    }


    private EditText setEditText(int id_hint) {
        LinearLayout.LayoutParams lEditParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, 120);
        EditText mainEdit = new EditText(getActivity());
        mainEdit.setLayoutParams(lEditParams);
        mainEdit.setHint(id_hint);
        return mainEdit;
    }


    private void setHeaderText(int indexOne, int indexTwo) {
        if (doit == 1) {
            header.setText(getString(indexOne));
        } else if (doit == 2) {
            header.setText(getString(indexTwo));
        }
    }


    private void initButton() {
        LinearLayout.LayoutParams lEditParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, 120);

        button_ok = new Button(getActivity());
        button_ok.setLayoutParams(lEditParams);
        button_ok.setText(getString(R.string.add_button_add_fragment));
        button_ok.setOnClickListener(getButtonPressAdd());
        layout.addView(button_ok);

        button_cancel = new Button(getActivity());
        button_cancel.setLayoutParams(lEditParams);
        button_cancel.setText(getString(R.string.cancel_add_fragment));
        button_cancel.setOnClickListener(getButtonPressCancel());
        layout.addView(button_cancel);
    }


    @NonNull
    private View.OnClickListener getButtonPressCancel() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.goToTheRecycleFragment();
            }
        };
    }


    @Override
    public void goToTheRecycleFragment() {
        FragmentRecycleView fragmentRecycleView = new FragmentRecycleView();
        Bundle bundle = new Bundle();
        bundle.putInt(KEY_INDEX, index_list);
        fragmentRecycleView.setArguments(bundle);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.addToBackStack(null);
        transaction.replace(R.id.field, fragmentRecycleView);
        transaction.commit();

        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm.isAcceptingText()) {
            imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }


    @NonNull
    private View.OnClickListener getButtonPressAdd() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (index_list) {
                    case 0:
                        presenter.addTown(doit);
                        break;

                    case 1:
                        presenter.addRoute(doit);
                        break;

                    case 2:
                        presenter.addDriver(doit);
                        break;

                    case 3:
                        presenter.addContract(doit);
                        break;

                    case 4:
                        presenter.addSheduled(doit);
                        break;

                    case 5:
                        presenter.addPointer(doit);
                        break;

                    case 6:
                        presenter.addCar(doit);
                        break;

                    case 7:
                        presenter.addInspection(doit);
                        break;

                    case 8:
                        presenter.addRepair(doit);
                        break;
                }
            }
        };
    }


    @Override
    public EntityTown getNewTown() {
        EntityTown entityTown = new EntityTown();
        entityTown.setName_town(editText1.getText().toString());
        return entityTown;
    }

    @Override
    public EntityTown getUpdateTown() {
        EntityTown entityTown = new EntityTown();
        entityTown.setName_town(editText1.getText().toString());
        entityTown.setId(record.get(position).getId());
        return entityTown;
    }

    @Override
    public EntityRoute getNewRoute() {
        EntityRoute entityRoute = new EntityRoute();

        try {
            entityRoute.setName_route(editText1.getText().toString());
            entityRoute.setLong_route(Integer.parseInt(editText2.getText().toString()));
            entityRoute.set_id_town(Integer.parseInt(editText3.getText().toString()));
        } catch (NumberFormatException e) {
            showToast("Ошибка ввода");
            return null;
        }

        return entityRoute;
    }

    @Override
    public EntityRoute getUpdateRoute() {
        EntityRoute entityRoute = new EntityRoute();
        try {
            entityRoute.setName_route(editText1.getText().toString());
            entityRoute.setLong_route(Integer.parseInt(editText2.getText().toString()));
            entityRoute.set_id_town(Integer.parseInt(editText3.getText().toString()));
            entityRoute.setId_route(record.get(position).getId());
        } catch (NumberFormatException e) {
            showToast("Ошибка ввода");
            return null;
        }
        return entityRoute;
    }


    @Override
    public EntityDriver getNewDriver() {

        EntityDriver entity = new EntityDriver();
        try {
            entity.setFiO(editText1.getText().toString());
            entity.setSalary(Integer.parseInt(editText2.getText().toString()));
            entity.setDrivers_license(editText3.getText().toString());
            entity.set_id_car(Integer.parseInt(editText4.getText().toString()));
            entity.set_id_contract(Integer.parseInt(editText5.getText().toString()));
        } catch (NumberFormatException e) {
            showToast("Ошибка ввода");
            return null;
        }

        return entity;
    }

    @Override
    public EntityDriver getUpdateDriver() {

        EntityDriver entity = new EntityDriver();
        try {
            entity.setFiO(editText1.getText().toString());
            entity.setSalary(Integer.parseInt(editText2.getText().toString()));
            entity.setDrivers_license(editText3.getText().toString());
            entity.set_id_car(Integer.parseInt(editText4.getText().toString()));
            entity.set_id_contract(Integer.parseInt(editText5.getText().toString()));
            entity.setId(record.get(position).getId());
        } catch (NumberFormatException e) {
            showToast("Ошибка ввода");
            return null;
        }

        return entity;
    }

    @Override
    public EntityContract getNewContract() {
        EntityContract entity = new EntityContract();
        try {
            entity.setDate_start(editText1.getText().toString());
            entity.setDate_finish(editText2.getText().toString());
        } catch (NumberFormatException e) {
            showToast("Ошибка ввода");
            return null;
        }

        return entity;
    }


    @Override
    public EntityContract getUpdateContract() {
        EntityContract entity = new EntityContract();
        try {
            entity.setDate_start(editText1.getText().toString());
            entity.setDate_finish(editText2.getText().toString());
            entity.setId(record.get(position).getId());
        } catch (NumberFormatException e) {
            showToast("Ошибка ввода");
            return null;
        }
        return entity;
    }

    @Override
    public EntityScheduledDeparture getNewSheduled() {
        EntityScheduledDeparture entity = new EntityScheduledDeparture();
        try {
            entity.setDate(editText1.getText().toString());
            entity.set_id_route(Integer.parseInt(editText2.getText().toString()));
            entity.set_id_stopping_point(Integer.parseInt(editText3.getText().toString()));
            entity.set_id_driver(Integer.parseInt(editText4.getText().toString()));
        } catch (NumberFormatException e) {
            showToast("Ошибка ввода");
            return null;
        }

        return entity;
    }

    @Override
    public EntityScheduledDeparture getUpdateSheduled() {
        EntityScheduledDeparture entity = new EntityScheduledDeparture();
        try {
            entity.setDate(editText1.getText().toString());
            entity.set_id_route(Integer.parseInt(editText2.getText().toString()));
            entity.set_id_stopping_point(Integer.parseInt(editText3.getText().toString()));
            entity.set_id_driver(Integer.parseInt(editText4.getText().toString()));
            entity.setId(record.get(position).getId());
        } catch (NumberFormatException e) {
            showToast("Ошибка ввода");
            return null;
        }

        return entity;
    }

    @Override
    public EntityStoppingPoint getNewPoint() {
        EntityStoppingPoint entity = new EntityStoppingPoint();
        try {
            entity.setName_stopping_point(editText1.getText().toString());
            entity.setAdress_stopping_point(editText2.getText().toString());
        } catch (NumberFormatException e) {
            showToast("Ошибка ввода");
            return null;
        }

        return entity;
    }

    @Override
    public EntityStoppingPoint getUpdatePoint() {
        EntityStoppingPoint entity = new EntityStoppingPoint();
        try {
            entity.setName_stopping_point(editText1.getText().toString());
            entity.setAdress_stopping_point(editText2.getText().toString());
            entity.setId(record.get(position).getId());
        } catch (NumberFormatException e) {
            showToast("Ошибка ввода");
            return null;
        }

        return entity;
    }

    @Override
    public EntityCar getNewCar() {
        EntityCar entity = new EntityCar();
        try {
            entity.setType_car(editText1.getText().toString());
            entity.setModel_car(editText2.getText().toString());
            entity.set_id_inspection(Integer.parseInt(editText3.getText().toString()));
            entity.set_id_repair(Integer.parseInt(editText4.getText().toString()));
        } catch (NumberFormatException e) {
            showToast("Ошибка ввода");
            return null;
        }

        return entity;
    }

    @Override
    public EntityCar getUpdateCar() {
        EntityCar entity = new EntityCar();
        try {
            entity.setType_car(editText1.getText().toString());
            entity.setModel_car(editText2.getText().toString());
            entity.set_id_inspection(Integer.parseInt(editText3.getText().toString()));
            entity.set_id_repair(Integer.parseInt(editText4.getText().toString()));
            entity.setId(record.get(position).getId());
        } catch (NumberFormatException e) {
            showToast("Ошибка ввода");
            return null;
        }

        return entity;
    }

    @Override
    public EntityInspection getNewInspection() {
        EntityInspection entity = new EntityInspection();
        try {
            entity.setDate_inspection(editText1.getText().toString());
            entity.set_result(editText2.getText().toString());
        } catch (NumberFormatException e) {
            showToast("Ошибка ввода");
            return null;
        }

        return entity;
    }

    @Override
    public EntityInspection getUpdateInspection() {
        EntityInspection entity = new EntityInspection();
        try {
            entity.setDate_inspection(editText1.getText().toString());
            entity.set_result(editText2.getText().toString());
            entity.setId(record.get(position).getId());
        } catch (NumberFormatException e) {
            showToast("Ошибка ввода");
            return null;
        }

        return entity;
    }

    @Override
    public EntityRepair getNewRepair() {
        EntityRepair entity = new EntityRepair();
        try {
            entity.setDate_repair(editText1.getText().toString());
            entity.setPrice_repair(Double.parseDouble(editText2.getText().toString()));
            entity.setDescription(editText3.getText().toString());
        } catch (NumberFormatException e) {
            showToast("Ошибка ввода");
            return null;
        }

        return entity;
    }

    @Override
    public EntityRepair getUpdateRepair() {
        EntityRepair entity = new EntityRepair();
        try {
            entity.setDate_repair(editText1.getText().toString());
            entity.setPrice_repair(Double.parseDouble(editText2.getText().toString()));
            entity.setDescription(editText3.getText().toString());
            entity.setId(record.get(position).getId());
        } catch (NumberFormatException e) {
            showToast("Ошибка ввода");
            return null;
        }

        return entity;
    }

    public void setListEntity(List<Entity> record) {
        this.record = record;
    }


    private void showLog(String show) {
        Log.d(TAG, show);
    }


    private void showToast(String str) {
        Toast toast = Toast.makeText(getActivity().getApplicationContext(),
                str, Toast.LENGTH_SHORT);
        toast.show();
    }
}
