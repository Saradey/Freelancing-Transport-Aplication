package transport.frilance.android8.sarad.transportaplication.FragmentMainMenu;


import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import transport.frilance.android8.sarad.transportaplication.FragmentRecycleView.FragmentRecycleView;
import transport.frilance.android8.sarad.transportaplication.R;

public class FragmentMainMenu extends Fragment implements MainView {

    private static final String KEY_INDEX = "key_index";
    private static final String TAG = "MyTag";
    PresenterMainMenu presenterMainMenu;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_menu, container, false);
        initUI(view);
        return view;
    }


    private void initUI(View view) {
        initButton(view);
        initPresenter();
    }


    private void initPresenter() {
        presenterMainMenu = new PresenterMainMenu(this);
    }

    private void initButton(View view) {
        createButton(view, R.id.goToTheTownList);
        createButton(view, R.id.goToTheRoute);
        createButton(view, R.id.goToTheDriver);
        createButton(view, R.id.goToTheContract);
        createButton(view, R.id.goToTheSheduled);
        createButton(view, R.id.goToThePointer);
        createButton(view, R.id.goToTheCar);
        createButton(view, R.id.goToTheInspection);
        createButton(view, R.id.goToTheRepait);
    }


    private void createButton(View view, int id_elem) {
        Button button = view.findViewById(id_elem);
        button.setOnClickListener(getOnClickListenerLambda());
    }

    @NonNull
    private View.OnClickListener getOnClickListenerLambda() {
        return v -> {
            switch (v.getId()) {
                case R.id.goToTheTownList:
                    presenterMainMenu.pressGoToTheTownList();
                    break;

                case R.id.goToTheRoute:
                    presenterMainMenu.pressgoToTheRoute();
                    break;

                case R.id.goToTheDriver:
                    presenterMainMenu.pressgoToTheDriver();
                    break;

                case R.id.goToTheContract:
                    presenterMainMenu.pressgoToTheContract();
                    break;

                case R.id.goToTheSheduled:
                    presenterMainMenu.pressgoToTheSheduled();
                    break;

                case R.id.goToThePointer:
                    presenterMainMenu.pressgoToThePointer();
                    break;

                case R.id.goToTheCar:
                    presenterMainMenu.pressgoToTheCar();
                    break;

                case R.id.goToTheInspection:
                    presenterMainMenu.pressgoToTheInspection();
                    break;

                case R.id.goToTheRepait:
                    presenterMainMenu.pressgoToTheRepair();
                    break;
            }
        };
    }




    @Override
    public void goToTheList(int index) {
        goToTheSomeFragment(index);
    }


    private void goToTheSomeFragment(int index) {
        FragmentRecycleView fragmentRecycleView = new FragmentRecycleView();
        Bundle bundle = new Bundle();
        bundle.putInt(KEY_INDEX, index);
        fragmentRecycleView.setArguments(bundle);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.addToBackStack(null);
        transaction.replace(R.id.field, fragmentRecycleView);
        transaction.commit();
    }

    private void showLog(String show) {
        Log.d(TAG, show);
    }
}
