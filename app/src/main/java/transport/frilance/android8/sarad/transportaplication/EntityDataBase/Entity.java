package transport.frilance.android8.sarad.transportaplication.EntityDataBase;

public interface Entity {

    int getId();

    void setId(int id);

}
