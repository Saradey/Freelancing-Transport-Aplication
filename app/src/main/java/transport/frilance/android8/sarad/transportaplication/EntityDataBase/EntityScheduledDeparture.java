package transport.frilance.android8.sarad.transportaplication.EntityDataBase;


public class EntityScheduledDeparture implements Entity {

    private static final String DATE = "Дата планового выезда: ";
    private static final String ID = "id планового выезда: ";
    private static final String ID_ROUTE = "id маршрута: ";
    private static final String ID_POINT = "id остановки: ";
    private static final String ID_DRIVER = "id водителя: ";
    private String date;
    private int id_scheduled_departure;

    private int _id_route;
    private int _id_stopping_point;
    private int _id_driver;

    public static String getDATE() {
        return DATE;
    }

    public static String getID() {
        return ID;
    }

    public int get_id_driver() {
        return _id_driver;
    }

    public void set_id_driver(int _id_driver) {
        this._id_driver = _id_driver;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getId_scheduled_departure() {
        return id_scheduled_departure;
    }

    public void setId_scheduled_departure(int id_scheduled_departure) {
        this.id_scheduled_departure = id_scheduled_departure;
    }

    public int get_id_route() {
        return _id_route;
    }

    public void set_id_route(int _id_route) {
        this._id_route = _id_route;
    }

    public int get_id_stopping_point() {
        return _id_stopping_point;
    }

    public void set_id_stopping_point(int _id_stopping_point) {
        this._id_stopping_point = _id_stopping_point;
    }

    @Override
    public int getId() {
        return id_scheduled_departure;
    }


    @Override
    public void setId(int id) {
        id_scheduled_departure = id;
    }


    @Override
    public String toString() {
        String result = ID + id_scheduled_departure + "\n\n" + DATE + date + "\n\n" + ID_ROUTE + _id_route
                + "\n\n" + ID_POINT + _id_stopping_point + "\n\n" + ID_DRIVER + _id_driver;
        return result;
    }

}
