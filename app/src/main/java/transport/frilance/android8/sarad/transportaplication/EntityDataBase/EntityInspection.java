package transport.frilance.android8.sarad.transportaplication.EntityDataBase;


public class EntityInspection implements Entity {

    private static final String RESULT = "Результат техосмотра: ";
    private static final String ID = "id техосмотра: ";
    private static final String DATE_INSPECTION = "дата техосмотра: ";
    private String _result;
    private int id_inspection;
    private String date_inspection;

    public static String getRESULT() {
        return RESULT;
    }

    public static String getID() {
        return ID;
    }

    public static String getDateInspection() {
        return DATE_INSPECTION;
    }

    public String get_result() {
        return _result;
    }

    public void set_result(String _result) {
        this._result = _result;
    }

    public int getId_inspection() {
        return id_inspection;
    }

    public void setId_inspection(int id_inspection) {
        this.id_inspection = id_inspection;
    }

    public String getDate_inspection() {
        return date_inspection;
    }

    public void setDate_inspection(String date_inspection) {
        this.date_inspection = date_inspection;
    }

    @Override
    public int getId() {
        return id_inspection;
    }


    @Override
    public void setId(int id) {
        id_inspection = id;
    }


    @Override
    public String toString() {
        String result = ID + id_inspection + "\n\n" + DATE_INSPECTION + date_inspection + "\n\n" +
                RESULT + _result;
        return result;
    }

}
