package transport.frilance.android8.sarad.transportaplication.EntityDataBase;


public class EntityCar implements Entity {

    private static final String TYPE = "Тип машины: ";
    private static final String ID = "id машины: ";
    private static final String MODEL = "Модель машины: ";
    private static final String ID_INS = "id техосмотра: ";
    private static final String ID_REP = "id ремонта: ";
    private String type_car;
    private int id_car;
    private String model_car;

    private int _id_inspection;
    private int _id_repair;

    public static String getTYPE() {
        return TYPE;
    }

    public static String getID() {
        return ID;
    }

    public static String getMODEL() {
        return MODEL;
    }

    public String getType_car() {
        return type_car;
    }

    public void setType_car(String type_car) {
        this.type_car = type_car;
    }

    public int getId_car() {
        return id_car;
    }

    public void setId_car(int id_car) {
        this.id_car = id_car;
    }

    public String getModel_car() {
        return model_car;
    }

    public void setModel_car(String model_car) {
        this.model_car = model_car;
    }

    public int get_id_inspection() {
        return _id_inspection;
    }

    public void set_id_inspection(int _id_inspection) {
        this._id_inspection = _id_inspection;
    }

    public int get_id_repair() {
        return _id_repair;
    }

    public void set_id_repair(int _id_repair) {
        this._id_repair = _id_repair;
    }

    @Override
    public int getId() {
        return id_car;
    }


    @Override
    public void setId(int id) {
        id_car = id;
    }


    @Override
    public String toString() {
        String result = ID + id_car + "\n\n" + TYPE + type_car + "\n\n" +
                MODEL + model_car + "\n\n" + ID_INS + _id_inspection + "\n\n" + ID_REP + _id_repair;
        return result;
    }

}
