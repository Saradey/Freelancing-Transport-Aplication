package transport.frilance.android8.sarad.transportaplication.TableDatabase;


public class TableStoppingPoint {


    private final String id_stopping_point = "id_stopping_point";
    private final String name_stopping_point = "name_stopping_point";
    private final String adress_stopping_point = "adress_stopping_point";
    private final String nameTable = "TableStoppingPoint";

    public String getId_stopping_point() {
        return id_stopping_point;
    }

    public String getName_stopping_point() {
        return name_stopping_point;
    }

    public String getAdress_stopping_point() {
        return adress_stopping_point;
    }

    public String getNameTable() {
        return nameTable;
    }
}
