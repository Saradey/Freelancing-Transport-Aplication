package transport.frilance.android8.sarad.transportaplication.FunctionalDB;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import transport.frilance.android8.sarad.transportaplication.EntityDataBase.Entity;
import transport.frilance.android8.sarad.transportaplication.EntityDataBase.EntityCar;
import transport.frilance.android8.sarad.transportaplication.EntityDataBase.EntityContract;
import transport.frilance.android8.sarad.transportaplication.EntityDataBase.EntityDriver;
import transport.frilance.android8.sarad.transportaplication.EntityDataBase.EntityInspection;
import transport.frilance.android8.sarad.transportaplication.EntityDataBase.EntityRepair;
import transport.frilance.android8.sarad.transportaplication.EntityDataBase.EntityRoute;
import transport.frilance.android8.sarad.transportaplication.EntityDataBase.EntityScheduledDeparture;
import transport.frilance.android8.sarad.transportaplication.EntityDataBase.EntityStoppingPoint;
import transport.frilance.android8.sarad.transportaplication.EntityDataBase.EntityTown;
import transport.frilance.android8.sarad.transportaplication.TableDatabase.WrapperAllTable;

public class SingletonNoteDataSource {

    private static final String TAG = "MyTag";

    private static SingletonNoteDataSource instance;
    private SQLiteDatabase database;
    private DatabaseHelper dbHelper;
    private WrapperAllTable wrapperAllTable;

    private SingletonNoteDataSource() {
    }


    public static SingletonNoteDataSource getInstance() {
        if (instance == null) {
            instance = new SingletonNoteDataSource();
        }
        return instance;
    }


    public void openDB(Context context) {
        if (dbHelper == null) {
            dbHelper = new DatabaseHelper(context);
            wrapperAllTable = dbHelper.getWrapperAllTable();
            database = dbHelper.getWritableDatabase();
        }
    }


    public void close() {
        dbHelper.close();
    }


    private void showLog(String show) {
        Log.d(TAG, show);
    }


    public List<Entity> getListTown() {

        List<Entity> listEntity = new ArrayList<>();
        String[] allColumn = {
                wrapperAllTable.getTableTown().getId_town(),
                wrapperAllTable.getTableTown().getName_town(),
        };
        Cursor cursor = database.query(wrapperAllTable.getTableTown().getNameTable(),
                allColumn, null, null, null, null, null);      //null, null, null это различные правила вывода
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            Entity note = cursorToTown(cursor);
            listEntity.add(note);
            cursor.moveToNext();
        }

        cursor.close();

        return listEntity;
    }


    private Entity cursorToTown(Cursor cursor) {
        EntityTown entity = new EntityTown();
        entity.setId(cursor.getInt(0));
        entity.setName_town(cursor.getString(1));
        return entity;
    }


    public void addTown(EntityTown town) {
        try {
            ContentValues values = new ContentValues(); //это обычный хэш мап
            values.put(wrapperAllTable.getTableTown().getName_town(), town.getName_town());
            database.insert(wrapperAllTable.getTableTown().getNameTable(), null, values);
        } catch (Exception e) {
            showLog("Error add addTown entity");
        }
    }


    public void updateTown(EntityTown town) {
        try {
            ContentValues values = new ContentValues(); //это обычный хэш мап
            values.put(wrapperAllTable.getTableTown().getName_town(), town.getName_town());   //добавили значение

            database.update(wrapperAllTable.getTableTown().getNameTable(),
                    values,
                    wrapperAllTable.getTableTown().getId_town() + "=" + town.getId(),
                    null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void deleteTown(int id) {
        database.delete(wrapperAllTable.getTableTown().getNameTable(), wrapperAllTable.getTableTown().getId_town()
                + " = " + id, null);
    }

///////////////////////////////////////////////////////////

    public List<Entity> getListInspection() {

        List<Entity> listEntity = new ArrayList<>();
        String[] allColumn = {
                wrapperAllTable.getTableInspection().getId_inspection(),
                wrapperAllTable.getTableInspection().getDate_inspection(),
                wrapperAllTable.getTableInspection().getResult()
        };
        Cursor cursor = database.query(wrapperAllTable.getTableInspection().getNameTable(),
                allColumn, null, null, null, null, null);      //null, null, null это различные правила вывода
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            Entity note = cursorToInspection(cursor);
            listEntity.add(note);
            cursor.moveToNext();
        }

        cursor.close();

        return listEntity;
    }


    private Entity cursorToInspection(Cursor cursor) {
        EntityInspection entity = new EntityInspection();
        entity.setId(cursor.getInt(0));
        entity.setDate_inspection(cursor.getString(1));
        entity.set_result(cursor.getString(2));
        return entity;
    }


    public void addInspection(EntityInspection inspection) {
        try {
            ContentValues values = new ContentValues(); //это обычный хэш мап
            values.put(wrapperAllTable.getTableInspection().getDate_inspection(), inspection.getDate_inspection());
            values.put(wrapperAllTable.getTableInspection().getResult(), inspection.get_result());

            database.insert(wrapperAllTable.getTableInspection().getNameTable(), null, values);
        } catch (Exception e) {
            showLog("Error add inspection entity");
        }
    }


    public void updateInspection(EntityInspection inspection) {
        try {
            ContentValues values = new ContentValues(); //это обычный хэш мап
            values.put(wrapperAllTable.getTableInspection().getDate_inspection(), inspection.getDate_inspection());
            values.put(wrapperAllTable.getTableInspection().getResult(), inspection.get_result());

            database.update(wrapperAllTable.getTableInspection().getNameTable(),
                    values,
                    wrapperAllTable.getTableInspection().getId_inspection() + "=" + inspection.getId(),
                    null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void deleteInspection(int id) {
        database.delete(wrapperAllTable.getTableInspection().getNameTable(), wrapperAllTable.getTableInspection().getId_inspection()
                + " = " + id, null);
    }

///////////////////////////////////////////////////////////

    public List<Entity> getListRepair() {

        List<Entity> listEntity = new ArrayList<>();
        String[] allColumn = {
                wrapperAllTable.getTableRepair().getId_repair(),
                wrapperAllTable.getTableRepair().getDate_repair(),
                wrapperAllTable.getTableRepair().getPrice_repair(),
                wrapperAllTable.getTableRepair().getDescription()
        };
        Cursor cursor = database.query(wrapperAllTable.getTableRepair().getNameTable(),
                allColumn, null, null, null, null, null);      //null, null, null это различные правила вывода
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            Entity note = cursorToRepair(cursor);
            listEntity.add(note);
            cursor.moveToNext();
        }

        cursor.close();

        return listEntity;
    }


    private Entity cursorToRepair(Cursor cursor) {
        EntityRepair entity = new EntityRepair();
        entity.setId(cursor.getInt(0));
        entity.setDate_repair(cursor.getString(1));
        entity.setPrice_repair(cursor.getDouble(2));
        entity.setDescription(cursor.getString(3));
        return entity;
    }


    public void addRepair(EntityRepair repair) {
        try {
            ContentValues values = new ContentValues(); //это обычный хэш мап
            values.put(wrapperAllTable.getTableRepair().getDate_repair(), repair.getDate_repair());
            values.put(wrapperAllTable.getTableRepair().getPrice_repair(), repair.getPrice_repair());
            values.put(wrapperAllTable.getTableRepair().getDescription(), repair.getDescription());

            database.insert(wrapperAllTable.getTableRepair().getNameTable(), null, values);
        } catch (Exception e) {
            showLog("Error add repair entity");
        }
    }


    public void updateRepair(EntityRepair repair) {
        try {
            ContentValues values = new ContentValues(); //это обычный хэш мап
            values.put(wrapperAllTable.getTableRepair().getDate_repair(), repair.getDate_repair());
            values.put(wrapperAllTable.getTableRepair().getPrice_repair(), repair.getPrice_repair());
            values.put(wrapperAllTable.getTableRepair().getDescription(), repair.getDescription());

            database.update(wrapperAllTable.getTableRepair().getNameTable(),
                    values,
                    wrapperAllTable.getTableRepair().getId_repair() + "=" + repair.getId(),
                    null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void deleteRepair(int id) {
        database.delete(wrapperAllTable.getTableRepair().getNameTable(), wrapperAllTable.getTableRepair().getId_repair()
                + " = " + id, null);
    }

///////////////////////////////////////////////////////////


    public List<Entity> getListCar() {

        List<Entity> listEntity = new ArrayList<>();
        String[] allColumn = {
                wrapperAllTable.getTableCar().getId_car(),
                wrapperAllTable.getTableCar().getType_car(),
                wrapperAllTable.getTableCar().getModel_car(),
                wrapperAllTable.getTableCar().get_id_inspection(),
                wrapperAllTable.getTableCar().get_id_repair()
        };

        Cursor cursor = database.query(wrapperAllTable.getTableCar().getNameTable(),
                allColumn, null, null, null, null, null);      //null, null, null это различные правила вывода
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            Entity note = cursorToCar(cursor);
            listEntity.add(note);
            cursor.moveToNext();
        }

        cursor.close();

        return listEntity;
    }


    private Entity cursorToCar(Cursor cursor) {
        EntityCar entity = new EntityCar();
        entity.setId(cursor.getInt(0));
        entity.setType_car(cursor.getString(1));
        entity.setModel_car(cursor.getString(2));
        entity.set_id_inspection(cursor.getInt(3));
        entity.set_id_repair(cursor.getInt(4));
        return entity;
    }


    public void addCar(EntityCar car) {
        try {
            ContentValues values = new ContentValues(); //это обычный хэш мап
            values.put(wrapperAllTable.getTableCar().getType_car(), car.getType_car());
            values.put(wrapperAllTable.getTableCar().getModel_car(), car.getModel_car());
            values.put(wrapperAllTable.getTableCar().get_id_inspection(), car.get_id_inspection());
            values.put(wrapperAllTable.getTableCar().get_id_repair(), car.get_id_repair());

            database.insert(wrapperAllTable.getTableCar().getNameTable(), null, values);
        } catch (Exception e) {
            showLog("Error add car entity");
        }
    }


    public void updateCar(EntityCar car) {
        try {
            ContentValues values = new ContentValues(); //это обычный хэш мап
            values.put(wrapperAllTable.getTableCar().getType_car(), car.getType_car());
            values.put(wrapperAllTable.getTableCar().getModel_car(), car.getModel_car());
            values.put(wrapperAllTable.getTableCar().get_id_inspection(), car.get_id_inspection());
            values.put(wrapperAllTable.getTableCar().get_id_repair(), car.get_id_repair());

            database.update(wrapperAllTable.getTableCar().getNameTable(),
                    values,
                    wrapperAllTable.getTableCar().getId_car() + "=" + car.getId(),
                    null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void deleteCar(int id) {
        database.delete(wrapperAllTable.getTableCar().getNameTable(), wrapperAllTable.getTableCar().getId_car()
                + " = " + id, null);
    }


///////////////////////////////////////////////////////////


    public List<Entity> getListStoppingPoint() {

        List<Entity> listEntity = new ArrayList<>();
        String[] allColumn = {
                wrapperAllTable.getTableStoppingPoint().getId_stopping_point(),
                wrapperAllTable.getTableStoppingPoint().getName_stopping_point(),
                wrapperAllTable.getTableStoppingPoint().getAdress_stopping_point()
        };

        Cursor cursor = database.query(wrapperAllTable.getTableStoppingPoint().getNameTable(),
                allColumn, null, null, null, null, null);      //null, null, null это различные правила вывода
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            Entity note = cursorToStoppingPoint(cursor);
            listEntity.add(note);
            cursor.moveToNext();
        }

        cursor.close();

        return listEntity;
    }


    private Entity cursorToStoppingPoint(Cursor cursor) {
        EntityStoppingPoint entity = new EntityStoppingPoint();
        entity.setId(cursor.getInt(0));
        entity.setName_stopping_point(cursor.getString(1));
        entity.setAdress_stopping_point(cursor.getString(2));
        return entity;
    }


    public void addStoppingPoint(EntityStoppingPoint stoppingPoint) {
        try {
            ContentValues values = new ContentValues(); //это обычный хэш мап
            values.put(wrapperAllTable.getTableStoppingPoint().getName_stopping_point(), stoppingPoint.getName_stopping_point());
            values.put(wrapperAllTable.getTableStoppingPoint().getAdress_stopping_point(), stoppingPoint.getAdress_stopping_point());

            database.insert(wrapperAllTable.getTableStoppingPoint().getNameTable(), null, values);

        } catch (Exception e) {
            showLog("Error add StoppingPoint entity");
        }
    }


    public void updateStoppingPoint(EntityStoppingPoint stoppingPoint) {
        try {
            ContentValues values = new ContentValues(); //это обычный хэш мап
            values.put(wrapperAllTable.getTableStoppingPoint().getName_stopping_point(), stoppingPoint.getName_stopping_point());
            values.put(wrapperAllTable.getTableStoppingPoint().getAdress_stopping_point(), stoppingPoint.getAdress_stopping_point());


            database.update(wrapperAllTable.getTableStoppingPoint().getNameTable(),
                    values,
                    wrapperAllTable.getTableStoppingPoint().getId_stopping_point() + "=" + stoppingPoint.getId(),
                    null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void deleteStoppingPoint(int id) {
        database.delete(wrapperAllTable.getTableStoppingPoint().getNameTable(), wrapperAllTable.getTableStoppingPoint().getId_stopping_point()
                + " = " + id, null);
    }

///////////////////////////////////////////////////////////


    public List<Entity> getListRoute() {

        List<Entity> listEntity = new ArrayList<>();
        String[] allColumn = {
                wrapperAllTable.getTableRoute().getId_route(),
                wrapperAllTable.getTableRoute().getName_route(),
                wrapperAllTable.getTableRoute().getLong_route(),
                wrapperAllTable.getTableRoute().get_id_town()
        };

        Cursor cursor = database.query(wrapperAllTable.getTableRoute().getNameTable(),
                allColumn, null, null, null, null, null);      //null, null, null это различные правила вывода
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            Entity note = cursorToRoute(cursor);
            listEntity.add(note);
            cursor.moveToNext();
        }

        cursor.close();

        return listEntity;
    }


    private Entity cursorToRoute(Cursor cursor) {
        EntityRoute entity = new EntityRoute();
        entity.setId(cursor.getInt(0));
        entity.setName_route(cursor.getString(1));
        entity.setLong_route(cursor.getInt(2));
        entity.set_id_town(cursor.getInt(3));
        return entity;
    }


    public void addRoute(EntityRoute route) {
        try {
            ContentValues values = new ContentValues(); //это обычный хэш мап
            values.put(wrapperAllTable.getTableRoute().getName_route(), route.getName_route());
            values.put(wrapperAllTable.getTableRoute().getLong_route(), route.getLong_route());
            values.put(wrapperAllTable.getTableRoute().get_id_town(), route.get_id_town());


            database.insert(wrapperAllTable.getTableRoute().getNameTable(), null, values);

        } catch (Exception e) {
            showLog("Error add route entity");
        }
    }


    public void updateRoute(EntityRoute route) {
        try {
            ContentValues values = new ContentValues(); //это обычный хэш мап
            values.put(wrapperAllTable.getTableRoute().getName_route(), route.getName_route());
            values.put(wrapperAllTable.getTableRoute().getLong_route(), route.getLong_route());
            values.put(wrapperAllTable.getTableRoute().get_id_town(), route.get_id_town());

            database.update(wrapperAllTable.getTableRoute().getNameTable(),
                    values,
                    wrapperAllTable.getTableRoute().getId_route() + "=" + route.getId(),
                    null);
        } catch (Exception e) {
            showLog("Error update route entity");
        }
    }


    public void deleteRoute(int id) {
        database.delete(wrapperAllTable.getTableRoute().getNameTable(), wrapperAllTable.getTableRoute().getId_route()
                + " = " + id, null);
    }

///////////////////////////////////////////////////////////


    public List<Entity> getListContract() {

        List<Entity> listEntity = new ArrayList<>();
        String[] allColumn = {
                wrapperAllTable.getTableContract().getId_contract(),
                wrapperAllTable.getTableContract().getDate_start(),
                wrapperAllTable.getTableContract().getDate_finish()
        };

        Cursor cursor = database.query(wrapperAllTable.getTableContract().getNameTable(),
                allColumn, null, null, null, null, null);      //null, null, null это различные правила вывода
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            Entity note = cursorToContract(cursor);
            listEntity.add(note);
            cursor.moveToNext();
        }

        cursor.close();

        return listEntity;
    }


    private Entity cursorToContract(Cursor cursor) {
        EntityContract entity = new EntityContract();
        entity.setId(cursor.getInt(0));
        entity.setDate_start(cursor.getString(1));
        entity.setDate_finish(cursor.getString(2));
        return entity;
    }


    public void addContract(EntityContract contract) {
        try {
            ContentValues values = new ContentValues(); //это обычный хэш мап
            values.put(wrapperAllTable.getTableContract().getDate_start(), contract.getDate_start());
            values.put(wrapperAllTable.getTableContract().getDate_finish(), contract.getDate_finish());

            database.insert(wrapperAllTable.getTableContract().getNameTable(), null, values);

        } catch (Exception e) {
            showLog("Error add contract entity");
        }
    }


    public void updateContract(EntityContract contract) {
        try {
            ContentValues values = new ContentValues(); //это обычный хэш мап
            values.put(wrapperAllTable.getTableContract().getDate_start(), contract.getDate_start());
            values.put(wrapperAllTable.getTableContract().getDate_finish(), contract.getDate_finish());

            database.update(wrapperAllTable.getTableContract().getNameTable(),
                    values,
                    wrapperAllTable.getTableContract().getId_contract() + "=" + contract.getId(),
                    null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void deleteContract(int id) {
        database.delete(wrapperAllTable.getTableContract().getNameTable(), wrapperAllTable.getTableContract().getId_contract()
                + " = " + id, null);
    }

///////////////////////////////////////////////////////////


    public List<Entity> getListScheduledDeparture() {

        List<Entity> listEntity = new ArrayList<>();
        String[] allColumn = {
                wrapperAllTable.getTableScheduledDeparture().getId_scheduled_departure(),
                wrapperAllTable.getTableScheduledDeparture().getDate(),
                wrapperAllTable.getTableScheduledDeparture().get_id_route(),
                wrapperAllTable.getTableScheduledDeparture().get_id_stopping_point(),
                wrapperAllTable.getTableScheduledDeparture().get_id_driver()
        };

        Cursor cursor = database.query(wrapperAllTable.getTableScheduledDeparture().getNameTable(),
                allColumn, null, null, null, null, null);      //null, null, null это различные правила вывода
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            Entity note = cursorToScheduledDeparture(cursor);
            listEntity.add(note);
            cursor.moveToNext();
        }

        cursor.close();

        return listEntity;
    }


    private Entity cursorToScheduledDeparture(Cursor cursor) {
        EntityScheduledDeparture entity = new EntityScheduledDeparture();
        entity.setId(cursor.getInt(0));
        entity.setDate(cursor.getString(1));
        entity.set_id_route(cursor.getInt(2));
        entity.set_id_stopping_point(cursor.getInt(3));
        entity.set_id_driver(cursor.getInt(4));
        return entity;
    }


    public void addScheduledDeparture(EntityScheduledDeparture scheduledDeparture) {
        try {
            ContentValues values = new ContentValues(); //это обычный хэш мап
            values.put(wrapperAllTable.getTableScheduledDeparture().getDate(),
                    scheduledDeparture.getDate());
            values.put(wrapperAllTable.getTableScheduledDeparture().get_id_route(),
                    scheduledDeparture.get_id_route());
            values.put(wrapperAllTable.getTableScheduledDeparture().get_id_stopping_point(),
                    scheduledDeparture.get_id_stopping_point());
            values.put(wrapperAllTable.getTableScheduledDeparture().get_id_driver(),
                    scheduledDeparture.get_id_driver());

            database.insert(wrapperAllTable.getTableScheduledDeparture().getNameTable(), null, values);

        } catch (Exception e) {
            showLog("Error add ScheduledDeparture entity");
        }
    }


    public void updateScheduledDeparture(EntityScheduledDeparture scheduledDeparture) {
        try {
            ContentValues values = new ContentValues(); //это обычный хэш мап
            values.put(wrapperAllTable.getTableScheduledDeparture().getDate(),
                    scheduledDeparture.getDate());
            values.put(wrapperAllTable.getTableScheduledDeparture().get_id_route(),
                    scheduledDeparture.get_id_route());
            values.put(wrapperAllTable.getTableScheduledDeparture().get_id_stopping_point(),
                    scheduledDeparture.get_id_stopping_point());
            values.put(wrapperAllTable.getTableScheduledDeparture().get_id_driver(),
                    scheduledDeparture.get_id_driver());

            database.update(wrapperAllTable.getTableScheduledDeparture().getNameTable(),
                    values,
                    wrapperAllTable.getTableScheduledDeparture().getId_scheduled_departure() + "=" +
                            scheduledDeparture.getId(), null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void deleteScheduledDeparture(int id) {
        database.delete(wrapperAllTable.getTableScheduledDeparture().getNameTable(),
                wrapperAllTable.getTableScheduledDeparture().getId_scheduled_departure()
                        + " = " + id, null);
    }


///////////////////////////////////////////////////////////


    public List<Entity> getListDriver() {

        List<Entity> listEntity = new ArrayList<>();
        String[] allColumn = {
                wrapperAllTable.getTableDriver().getId_driver(),
                wrapperAllTable.getTableDriver().getFiO(),
                wrapperAllTable.getTableDriver().getSalary(),
                wrapperAllTable.getTableDriver().getDrivers_license(),
                wrapperAllTable.getTableDriver().get_id_contract(),
                wrapperAllTable.getTableDriver().get_id_car()
        };

        Cursor cursor = database.query(wrapperAllTable.getTableDriver().getNameTable(),
                allColumn, null, null, null, null, null);      //null, null, null это различные правила вывода
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            Entity note = cursorToDriver(cursor);
            listEntity.add(note);
            cursor.moveToNext();
        }

        cursor.close();

        return listEntity;
    }


    private Entity cursorToDriver(Cursor cursor) {
        EntityDriver entity = new EntityDriver();
        entity.setId(cursor.getInt(0));
        entity.setFiO(cursor.getString(1));
        entity.setSalary(cursor.getDouble(2));
        entity.setDrivers_license(cursor.getString(3));
        entity.set_id_contract(cursor.getInt(4));
        entity.set_id_car(cursor.getInt(5));
        return entity;
    }


    public void addDriver(EntityDriver entity) {
        try {
            ContentValues values = new ContentValues(); //это обычный хэш мап
            values.put(wrapperAllTable.getTableDriver().getFiO(), entity.getFiO());
            values.put(wrapperAllTable.getTableDriver().getSalary(), entity.getSalary());
            values.put(wrapperAllTable.getTableDriver().getDrivers_license(), entity.getDrivers_license());
            values.put(wrapperAllTable.getTableDriver().get_id_contract(), entity.get_id_contract());
            values.put(wrapperAllTable.getTableDriver().get_id_car(), entity.get_id_car());


            database.insert(wrapperAllTable.getTableDriver().getNameTable(), null, values);

        } catch (Exception e) {
            showLog("Error add EntityDriver entity");
        }
    }


    public void updateDriver(EntityDriver entity) {
        try {
            ContentValues values = new ContentValues(); //это обычный хэш мап
            values.put(wrapperAllTable.getTableDriver().getFiO(), entity.getFiO());
            values.put(wrapperAllTable.getTableDriver().getSalary(), entity.getSalary());
            values.put(wrapperAllTable.getTableDriver().getDrivers_license(), entity.getDrivers_license());
            values.put(wrapperAllTable.getTableDriver().get_id_contract(), entity.get_id_contract());
            values.put(wrapperAllTable.getTableDriver().get_id_car(), entity.get_id_car());

            database.update(wrapperAllTable.getTableDriver().getNameTable(),
                    values,
                    wrapperAllTable.getTableDriver().getId_driver() + "=" +
                            entity.getId(), null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void deleteDriver(int id) {
        database.delete(wrapperAllTable.getTableDriver().getNameTable(),
                wrapperAllTable.getTableDriver().getId_driver()
                        + " = " + id, null);
    }

}
