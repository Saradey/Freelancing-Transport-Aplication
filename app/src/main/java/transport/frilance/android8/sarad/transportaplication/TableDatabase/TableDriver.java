package transport.frilance.android8.sarad.transportaplication.TableDatabase;


public class TableDriver {

    private final String id_driver = "id_driver";
    private final String FiO = "FiO";
    private final String salary = "salary";
    private final String drivers_license = "drivers_license ";
    private final String _id_car = "_id_car";
    private final String _id_contract = "_id_contract";
    private final String nameTable = "Driver";


    public String getId_driver() {
        return id_driver;
    }

    public String getFiO() {
        return FiO;
    }

    public String getSalary() {
        return salary;
    }

    public String getDrivers_license() {
        return drivers_license;
    }

    public String get_id_car() {
        return _id_car;
    }

    public String get_id_contract() {
        return _id_contract;
    }

    public String getNameTable() {
        return nameTable;
    }
}
