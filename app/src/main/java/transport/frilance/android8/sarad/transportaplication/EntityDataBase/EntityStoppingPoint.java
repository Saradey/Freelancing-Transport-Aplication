package transport.frilance.android8.sarad.transportaplication.EntityDataBase;


public class EntityStoppingPoint implements Entity {

    private static final String NAME = "Навание остановочного пункта: ";
    private static final String ID = "id остановочного пункта: ";
    private static final String ADRESS = "Адрес остановочного пункта: ";
    private String name_stopping_point;
    private int id_stopping_point;
    private String adress_stopping_point;

    public static String getNAME() {
        return NAME;
    }

    public static String getID() {
        return ID;
    }

    public static String getADRESS() {
        return ADRESS;
    }

    public String getName_stopping_point() {
        return name_stopping_point;
    }

    public void setName_stopping_point(String name_stopping_point) {
        this.name_stopping_point = name_stopping_point;
    }

    public int getId_stopping_point() {
        return id_stopping_point;
    }

    public void setId_stopping_point(int id_stopping_point) {
        this.id_stopping_point = id_stopping_point;
    }

    public String getAdress_stopping_point() {
        return adress_stopping_point;
    }

    public void setAdress_stopping_point(String adress_stopping_point) {
        this.adress_stopping_point = adress_stopping_point;
    }

    @Override
    public int getId() {
        return id_stopping_point;
    }


    @Override
    public void setId(int id) {
        id_stopping_point = id;
    }


    @Override
    public String toString() {
        String result = ID + id_stopping_point + "\n\n" + NAME + name_stopping_point + "\n\n"
                + ADRESS + adress_stopping_point;
        return result;
    }

}
