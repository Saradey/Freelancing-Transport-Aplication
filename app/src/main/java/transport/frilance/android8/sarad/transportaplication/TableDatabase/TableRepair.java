package transport.frilance.android8.sarad.transportaplication.TableDatabase;

public class TableRepair {

    private final String id_repair = "id_repair";
    private final String date_repair = "date_repair";
    private final String price_repair = "price_repair";
    private final String description = "description";
    private final String nameTable = "TableRepair";

    public String getId_repair() {
        return id_repair;
    }

    public String getDate_repair() {
        return date_repair;
    }

    public String getPrice_repair() {
        return price_repair;
    }

    public String getDescription() {
        return description;
    }

    public String getNameTable() {
        return nameTable;
    }
}
