package transport.frilance.android8.sarad.transportaplication.FragmentAdd;


import transport.frilance.android8.sarad.transportaplication.EntityDataBase.EntityCar;
import transport.frilance.android8.sarad.transportaplication.EntityDataBase.EntityContract;
import transport.frilance.android8.sarad.transportaplication.EntityDataBase.EntityDriver;
import transport.frilance.android8.sarad.transportaplication.EntityDataBase.EntityInspection;
import transport.frilance.android8.sarad.transportaplication.EntityDataBase.EntityRepair;
import transport.frilance.android8.sarad.transportaplication.EntityDataBase.EntityRoute;
import transport.frilance.android8.sarad.transportaplication.EntityDataBase.EntityScheduledDeparture;
import transport.frilance.android8.sarad.transportaplication.EntityDataBase.EntityStoppingPoint;
import transport.frilance.android8.sarad.transportaplication.EntityDataBase.EntityTown;

public interface InterfaceFragmentAdd {


    EntityTown getNewTown();

    EntityTown getUpdateTown();


    EntityRoute getNewRoute();

    EntityRoute getUpdateRoute();


    EntityDriver getNewDriver();

    EntityDriver getUpdateDriver();


    EntityContract getNewContract();

    EntityContract getUpdateContract();


    EntityScheduledDeparture getNewSheduled();

    EntityScheduledDeparture getUpdateSheduled();


    EntityStoppingPoint getNewPoint();

    EntityStoppingPoint getUpdatePoint();


    EntityCar getNewCar();

    EntityCar getUpdateCar();


    EntityInspection getNewInspection();

    EntityInspection getUpdateInspection();


    EntityRepair getNewRepair();

    EntityRepair getUpdateRepair();


    void goToTheRecycleFragment();

}
