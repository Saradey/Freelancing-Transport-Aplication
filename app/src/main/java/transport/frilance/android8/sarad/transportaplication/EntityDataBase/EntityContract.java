package transport.frilance.android8.sarad.transportaplication.EntityDataBase;

public class EntityContract implements Entity {

    private static final String DATE_START = "Дата заключения контракта: ";
    private static final String ID = "id контракта: ";
    private static final String DATE_FINISH = "Дата окончания контракта: ";
    private String date_start;
    private int id_contract;
    private String date_finish;

    public static String getDateStart() {
        return DATE_START;
    }

    public static String getID() {
        return ID;
    }

    public static String getDateFinish() {
        return DATE_FINISH;
    }

    public String getDate_start() {
        return date_start;
    }

    public void setDate_start(String date_start) {
        this.date_start = date_start;
    }

    public int getId_contract() {
        return id_contract;
    }

    public void setId_contract(int id_contract) {
        this.id_contract = id_contract;
    }

    public String getDate_finish() {
        return date_finish;
    }

    public void setDate_finish(String date_finish) {
        this.date_finish = date_finish;
    }

    @Override
    public int getId() {
        return id_contract;
    }


    @Override
    public void setId(int id) {
        id_contract = id;
    }


    @Override
    public String toString() {
        String result = ID + id_contract + "\n\n" + DATE_START + date_start + "\n\n" +
                DATE_FINISH + date_finish;
        return result;
    }

}
