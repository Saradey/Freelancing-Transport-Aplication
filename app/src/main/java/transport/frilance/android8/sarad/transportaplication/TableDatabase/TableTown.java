package transport.frilance.android8.sarad.transportaplication.TableDatabase;


public class TableTown {

    private final String id_town = "id_town";
    private final String name_town = "name_town";
    private final String nameTable = "EntityTown";


    public String getNameTable() {
        return nameTable;
    }

    public String getId_town() {
        return id_town;
    }

    public String getName_town() {
        return name_town;
    }

}
