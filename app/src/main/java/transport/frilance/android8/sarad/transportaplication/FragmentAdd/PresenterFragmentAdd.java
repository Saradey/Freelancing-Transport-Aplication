package transport.frilance.android8.sarad.transportaplication.FragmentAdd;

public class PresenterFragmentAdd {

    private InterfaceFragmentAdd myView;
    private ModelFragmentAdd model;


    PresenterFragmentAdd(InterfaceFragmentAdd myView) {
        this.myView = myView;
        this.model = new ModelFragmentAdd();
    }


    public void addTown() {
        model.addTown(myView.getNewTown());
        myView.goToTheRecycleFragment();
    }


    public void goToTheRecycleFragment() {
        myView.goToTheRecycleFragment();
    }


    public void updateTown() {
        model.updateTown(myView.getUpdateTown());
        myView.goToTheRecycleFragment();
    }


    public void addTown(int doit) {
        if (doit == 1) {
            addTown();
        } else if (doit == 2) {
            updateTown();
        }
    }


    public void addRoute(int doit) {
        if (doit == 1) {
            addNewRoute();
        } else if (doit == 2) {
            updateRoute();
        }
    }

    private void updateRoute() {
        model.updateRoute(myView.getUpdateRoute());
        myView.goToTheRecycleFragment();
    }

    private void addNewRoute() {
        model.addRoute(myView.getNewRoute());
        myView.goToTheRecycleFragment();
    }

    public void addDriver(int doit) {
        if (doit == 1) {
            addnewDriver();
        } else if (doit == 2) {
            updateDriver();
        }
    }

    private void updateDriver() {
        model.updateDriver(myView.getUpdateDriver());
        myView.goToTheRecycleFragment();
    }

    private void addnewDriver() {
        model.addDriver(myView.getNewDriver());
        myView.goToTheRecycleFragment();
    }

    public void addContract(int doit) {
        if (doit == 1) {
            addNewContract();
        } else if (doit == 2) {
            updateContract();
        }
    }

    private void updateContract() {
        model.updateContract(myView.getUpdateContract());
        myView.goToTheRecycleFragment();
    }

    private void addNewContract() {
        model.addContract(myView.getNewContract());
        myView.goToTheRecycleFragment();
    }

    public void addSheduled(int doit) {
        if (doit == 1) {
            addNewSheduled();
        } else if (doit == 2) {
            updateSheduled();
        }
    }

    private void updateSheduled() {
        model.updateSheduled(myView.getUpdateSheduled());
        myView.goToTheRecycleFragment();
    }

    private void addNewSheduled() {
        model.addSheduled(myView.getNewSheduled());
        myView.goToTheRecycleFragment();
    }

    public void addPointer(int doit) {
        if (doit == 1) {
            addNewPointer();
        } else if (doit == 2) {
            updatePointer();
        }
    }

    private void updatePointer() {
        model.updatePointer(myView.getUpdatePoint());
        myView.goToTheRecycleFragment();
    }

    private void addNewPointer() {
        model.addPointer(myView.getNewPoint());
        myView.goToTheRecycleFragment();
    }

    public void addCar(int doit) {
        if (doit == 1) {
            addNewCar();
        } else if (doit == 2) {
            updateCar();
        }
    }

    private void updateCar() {
        model.updateCar(myView.getUpdateCar());
        myView.goToTheRecycleFragment();
    }

    private void addNewCar() {
        model.addCar(myView.getNewCar());
        myView.goToTheRecycleFragment();
    }

    public void addInspection(int doit) {
        if (doit == 1) {
            addNewInspection();
        } else if (doit == 2) {
            updateInspection();
        }
    }

    private void updateInspection() {
        model.updateInspection(myView.getUpdateInspection());
        myView.goToTheRecycleFragment();
    }

    private void addNewInspection() {
        model.addInspection(myView.getNewInspection());
        myView.goToTheRecycleFragment();
    }


    public void addRepair(int doit) {
        if (doit == 1) {
            addNewRepair();
        } else if (doit == 2) {
            updateRepair();
        }
    }

    private void updateRepair() {
        model.updateRepair(myView.getUpdateRepair());
        myView.goToTheRecycleFragment();
    }

    private void addNewRepair() {
        model.addRepair(myView.getNewRepair());
        myView.goToTheRecycleFragment();
    }
}
