package transport.frilance.android8.sarad.transportaplication.TableDatabase;


public class TableCar {

    private final String id_car = "id_car";
    private final String type_car = "type_car";
    private final String model_car = "model_car";
    private final String _id_inspection = "_id_inspection";
    private final String _id_repair = "_id_repair";
    private final String nameTable = "TableCar";


    public String getId_car() {
        return id_car;
    }

    public String getType_car() {
        return type_car;
    }

    public String getModel_car() {
        return model_car;
    }

    public String get_id_inspection() {
        return _id_inspection;
    }

    public String get_id_repair() {
        return _id_repair;
    }

    public String getNameTable() {
        return nameTable;
    }
}
