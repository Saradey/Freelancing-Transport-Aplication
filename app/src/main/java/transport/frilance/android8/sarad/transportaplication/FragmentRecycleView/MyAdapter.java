package transport.frilance.android8.sarad.transportaplication.FragmentRecycleView;


import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import transport.frilance.android8.sarad.transportaplication.EntityDataBase.Entity;
import transport.frilance.android8.sarad.transportaplication.R;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private static RecyclerViewClickListener itemListener;
    private List<Entity> records;

    MyAdapter(List<Entity> records, RecyclerViewClickListener itemListener) {
        this.records = records;
        this.itemListener = itemListener;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.my_recycle, viewGroup, false);

        return new MyViewHolder(v);
    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        String record = records.get(position).toString();
        holder.bind(record);
    }


    @Override
    public int getItemCount() {
        return records.size();
    }


    public void remove(int position) {
        records.remove(position);
        notifyItemRemoved(position);
    }


    public void add(Entity string) {
        records.add(string);
        notifyItemInserted(records.size());
    }


    public void update(int position, Entity teamp) {
        records.set(position, teamp);
        notifyItemChanged(position);
    }



    public List<Entity> getList() {
        return records;
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {

        @BindView(R.id.items)
        TextView contentInformation;


        MyViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
            v.setOnCreateContextMenuListener(this);
        }

        private void bind(String record) {
            contentInformation.setText(record);
        }


        @Override
        public void onCreateContextMenu(ContextMenu menu, View v,
                                        ContextMenu.ContextMenuInfo menuInfo) {

            menu.add(Menu.NONE, R.id.menu_edit, 1, v.getResources().getString(R.string.upd));//groupId, itemId, order, title
            menu.add(Menu.NONE, R.id.menu_delete, 2, v.getResources().getString(R.string.del));

            itemListener.recyclerViewListClicked(v, this.getLayoutPosition());
        }


    }

}
