package transport.frilance.android8.sarad.transportaplication.FragmentRecycleView;

import android.view.View;

public interface RecyclerViewClickListener {

    void recyclerViewListClicked(View v, int position);

}
