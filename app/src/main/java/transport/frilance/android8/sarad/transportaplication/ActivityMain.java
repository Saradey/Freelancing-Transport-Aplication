package transport.frilance.android8.sarad.transportaplication;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import transport.frilance.android8.sarad.transportaplication.FragmentMainMenu.FragmentMainMenu;
import transport.frilance.android8.sarad.transportaplication.FunctionalDB.SingletonNoteDataSource;


public class ActivityMain extends AppCompatActivity {


    private static final String TAG = "MyTag";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initMainMenu();
        initDb();
    }

    private void initDb() {
        SingletonNoteDataSource.getInstance().openDB(this);
    }


    private void initMainMenu() {
        FragmentMainMenu fragmentMainMenu = new FragmentMainMenu();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.addToBackStack(null);
        transaction.replace(R.id.field, fragmentMainMenu);
        transaction.commit();
    }


}
