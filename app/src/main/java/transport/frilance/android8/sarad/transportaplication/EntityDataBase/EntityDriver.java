package transport.frilance.android8.sarad.transportaplication.EntityDataBase;


public class EntityDriver implements Entity {

    private static final String FIO = "Фамилия имя отчество: ";
    private static final String ID = "id водителя: ";
    private static final String SALARY = "Зарплата водителя: ";
    private static final String LICENSE = "Водительское удостоверение: ";
    private static final String ID_CAR = "id машины: ";
    private static final String ID_CONTRACT = "id контракта: ";
    private String FiO;
    private int id_driver;
    private double salary;
    private String drivers_license;

    private int _id_car;
    private int _id_contract;

    public static String getFIO() {
        return FIO;
    }

    public static String getID() {
        return ID;
    }

    public static String getSALARY() {
        return SALARY;
    }

    public static String getLICENSE() {
        return LICENSE;
    }

    public String getFiO() {
        return FiO;
    }

    public void setFiO(String fiO) {
        FiO = fiO;
    }

    public int getId_driver() {
        return id_driver;
    }

    public void setId_driver(int id_driver) {
        this.id_driver = id_driver;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String getDrivers_license() {
        return drivers_license;
    }

    public void setDrivers_license(String drivers_license) {
        this.drivers_license = drivers_license;
    }

    public int get_id_car() {
        return _id_car;
    }

    public void set_id_car(int _id_car) {
        this._id_car = _id_car;
    }

    public int get_id_contract() {
        return _id_contract;
    }

    public void set_id_contract(int _id_contract) {
        this._id_contract = _id_contract;
    }

    @Override
    public int getId() {
        return id_driver;
    }


    @Override
    public void setId(int id) {
        id_driver = id;
    }


    @Override
    public String toString() {
        String result = ID + id_driver + "\n\n" + FIO + FiO + "\n\n" +
                SALARY + salary + "\n\n" + LICENSE + drivers_license + "\n\n" + ID_CONTRACT + _id_contract
                + "\n\n" + ID_CAR + _id_car;
        return result;
    }

}
