package transport.frilance.android8.sarad.transportaplication.FragmentMainMenu;

class PresenterMainMenu {

    MainView mainView;

    PresenterMainMenu(MainView mainView) {
        this.mainView = mainView;
    }


    public void pressGoToTheTownList() {
        mainView.goToTheList(0);
    }

    public void pressgoToTheRoute() {
        mainView.goToTheList(1);
    }

    public void pressgoToTheDriver() {
        mainView.goToTheList(2);
    }

    public void pressgoToTheContract() {
        mainView.goToTheList(3);
    }

    public void pressgoToTheSheduled() {
        mainView.goToTheList(4);
    }

    public void pressgoToThePointer() {
        mainView.goToTheList(5);
    }

    public void pressgoToTheCar() {
        mainView.goToTheList(6);
    }

    public void pressgoToTheInspection() {
        mainView.goToTheList(7);
    }

    public void pressgoToTheRepair() {
        mainView.goToTheList(8);
    }
}
