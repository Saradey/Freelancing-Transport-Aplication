package transport.frilance.android8.sarad.transportaplication.TableDatabase;


public class TableContract {

    private final String id_contract = "id_contract";
    private final String date_start = "date_start";
    private final String date_finish = "date_finish";
    private final String nameTable = "Contract";

    public String getId_contract() {
        return id_contract;
    }

    public String getDate_start() {
        return date_start;
    }

    public String getDate_finish() {
        return date_finish;
    }

    public String getNameTable() {
        return nameTable;
    }
}
