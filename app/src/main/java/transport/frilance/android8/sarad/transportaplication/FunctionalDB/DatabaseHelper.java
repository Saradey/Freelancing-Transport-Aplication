package transport.frilance.android8.sarad.transportaplication.FunctionalDB;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import transport.frilance.android8.sarad.transportaplication.TableDatabase.WrapperAllTable;


public class DatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "myDateBase.db";

    private WrapperAllTable wrapperAllTable = new WrapperAllTable();

    private static final String TAG = "MyTag";


    DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL("CREATE TABLE "
                    + wrapperAllTable.getTableTown().getNameTable()
                    + " ("
                    + wrapperAllTable.getTableTown().getId_town()
                    + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + wrapperAllTable.getTableTown().getName_town()
                    + " TEXT"
                    + ");");


            db.execSQL("CREATE TABLE "
                    + wrapperAllTable.getTableRoute().getNameTable()
                    + " ("
                    + wrapperAllTable.getTableRoute().getId_route()
                    + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + wrapperAllTable.getTableRoute().getName_route()
                    + " TEXT,"
                    + wrapperAllTable.getTableRoute().getLong_route()
                    + " INTEGER,"
                    + wrapperAllTable.getTableRoute().get_id_town()
                    + " INTEGER,"
                    + "FOREIGN KEY (" + wrapperAllTable.getTableRoute().get_id_town() + ") "
                    + "REFERENCES " + wrapperAllTable.getTableTown().getNameTable()
                    + " (" + wrapperAllTable.getTableTown().getId_town() + ") "
                    + "ON DELETE CASCADE ON UPDATE CASCADE"
                    + ");");


            db.execSQL("CREATE TABLE "
                    + wrapperAllTable.getTableStoppingPoint().getNameTable()
                    + " ("
                    + wrapperAllTable.getTableStoppingPoint().getId_stopping_point()
                    + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + wrapperAllTable.getTableStoppingPoint().getName_stopping_point()
                    + " TEXT,"
                    + wrapperAllTable.getTableStoppingPoint().getAdress_stopping_point()
                    + " TEXT"
                    + ");");


            db.execSQL("CREATE TABLE "
                    + wrapperAllTable.getTableInspection().getNameTable()
                    + " ("
                    + wrapperAllTable.getTableInspection().getId_inspection()
                    + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + wrapperAllTable.getTableInspection().getDate_inspection()
                    + " TEXT,"
                    + wrapperAllTable.getTableInspection().getResult()
                    + " TEXT"
                    + ");");


            db.execSQL("CREATE TABLE "
                    + wrapperAllTable.getTableRepair().getNameTable()
                    + " ("
                    + wrapperAllTable.getTableRepair().getId_repair()
                    + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + wrapperAllTable.getTableRepair().getDate_repair()
                    + " TEXT,"
                    + wrapperAllTable.getTableRepair().getDescription()
                    + " TEXT,"
                    + wrapperAllTable.getTableRepair().getPrice_repair()
                    + " REAL"
                    + ");");


            db.execSQL("CREATE TABLE "
                    + wrapperAllTable.getTableContract().getNameTable()
                    + " ("
                    + wrapperAllTable.getTableContract().getId_contract()
                    + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + wrapperAllTable.getTableContract().getDate_start()
                    + " TEXT,"
                    + wrapperAllTable.getTableContract().getDate_finish()
                    + " TEXT"
                    + ");");


            db.execSQL("CREATE TABLE "
                    + wrapperAllTable.getTableCar().getNameTable()
                    + " ("
                    + wrapperAllTable.getTableCar().getId_car()
                    + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + wrapperAllTable.getTableCar().getType_car()
                    + " TEXT,"
                    + wrapperAllTable.getTableCar().getModel_car()
                    + " TEXT,"
                    + wrapperAllTable.getTableCar().get_id_inspection()
                    + " INTEGER,"
                    + wrapperAllTable.getTableCar().get_id_repair()
                    + " INTEGER,"
                    + "FOREIGN KEY (" + wrapperAllTable.getTableCar().get_id_inspection() + ") "
                    + "REFERENCES " + wrapperAllTable.getTableInspection().getNameTable()
                    + " (" + wrapperAllTable.getTableInspection().getId_inspection() + ") "
                    + "ON DELETE CASCADE ON UPDATE CASCADE,"
                    + "FOREIGN KEY (" + wrapperAllTable.getTableCar().get_id_repair() + ") "
                    + "REFERENCES " + wrapperAllTable.getTableRepair().getNameTable()
                    + " (" + wrapperAllTable.getTableRepair().getId_repair() + ") "
                    + "ON DELETE CASCADE ON UPDATE CASCADE"
                    + ");");


            db.execSQL("CREATE TABLE "
                    + wrapperAllTable.getTableDriver().getNameTable()
                    + " ("
                    + wrapperAllTable.getTableDriver().getId_driver()
                    + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + wrapperAllTable.getTableDriver().getFiO()
                    + " TEXT,"
                    + wrapperAllTable.getTableDriver().getSalary()
                    + " REAL,"
                    + wrapperAllTable.getTableDriver().getDrivers_license()
                    + " TEXT,"
                    + wrapperAllTable.getTableDriver().get_id_car()
                    + " INTEGER,"
                    + wrapperAllTable.getTableDriver().get_id_contract()
                    + " INTEGER,"
                    + "FOREIGN KEY (" + wrapperAllTable.getTableDriver().get_id_car() + ") "
                    + "REFERENCES " + wrapperAllTable.getTableCar().getNameTable()
                    + " (" + wrapperAllTable.getTableCar().getId_car() + ") "
                    + "ON DELETE CASCADE ON UPDATE CASCADE,"
                    + "FOREIGN KEY (" + wrapperAllTable.getTableDriver().get_id_contract() + ") "
                    + "REFERENCES " + wrapperAllTable.getTableContract().getNameTable()
                    + " (" + wrapperAllTable.getTableContract().getId_contract() + ") "
                    + "ON DELETE CASCADE ON UPDATE CASCADE"
                    + ");");


            db.execSQL("CREATE TABLE "
                    + wrapperAllTable.getTableScheduledDeparture().getNameTable()
                    + " ("
                    + wrapperAllTable.getTableScheduledDeparture().getId_scheduled_departure()
                    + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + wrapperAllTable.getTableScheduledDeparture().getDate()
                    + " TEXT,"
                    + wrapperAllTable.getTableScheduledDeparture().get_id_route()
                    + " INTEGER,"
                    + wrapperAllTable.getTableScheduledDeparture().get_id_stopping_point()
                    + " INTEGER,"
                    + wrapperAllTable.getTableScheduledDeparture().get_id_driver()
                    + " INTEGER,"
                    + "FOREIGN KEY (" + wrapperAllTable.getTableScheduledDeparture().get_id_route() + ") "
                    + "REFERENCES " + wrapperAllTable.getTableRoute().getNameTable()
                    + " (" + wrapperAllTable.getTableRoute().getId_route() + ") "
                    + "ON DELETE CASCADE ON UPDATE CASCADE,"
                    + "FOREIGN KEY (" + wrapperAllTable.getTableScheduledDeparture().get_id_stopping_point() + ") "
                    + "REFERENCES " + wrapperAllTable.getTableStoppingPoint().getNameTable()
                    + " (" + wrapperAllTable.getTableStoppingPoint().getId_stopping_point() + ") "
                    + "ON DELETE CASCADE ON UPDATE CASCADE,"
                    + "FOREIGN KEY (" + wrapperAllTable.getTableScheduledDeparture().get_id_driver() + ") "
                    + "REFERENCES " + wrapperAllTable.getTableDriver().getNameTable()
                    + " (" + wrapperAllTable.getTableDriver().getId_driver() + ") "
                    + "ON DELETE CASCADE ON UPDATE CASCADE"
                    + ");");


        } catch (SQLException e) {
            Log.d(TAG, "Error Create db");
            e.printStackTrace();
        }


    }


    public WrapperAllTable getWrapperAllTable() {
        return wrapperAllTable;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //здесь изменения версии
    }

}
