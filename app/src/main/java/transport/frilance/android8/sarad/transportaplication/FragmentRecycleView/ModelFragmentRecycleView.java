package transport.frilance.android8.sarad.transportaplication.FragmentRecycleView;


import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import transport.frilance.android8.sarad.transportaplication.EntityDataBase.Entity;
import transport.frilance.android8.sarad.transportaplication.FunctionalDB.SingletonNoteDataSource;


public class ModelFragmentRecycleView {

    SingletonNoteDataSource noteDataSource;

    private static final String TAG = "MyTag";

    ModelFragmentRecycleView() {
        noteDataSource = SingletonNoteDataSource.getInstance();
    }


    public List<Entity> getList(int index) {
        List<Entity> myList = new ArrayList<>();

        switch (index) {
            case 0:
                myList = noteDataSource.getListTown();
                break;

            case 1:
                myList = noteDataSource.getListRoute();
                break;

            case 2:
                myList = noteDataSource.getListDriver();
                break;

            case 3:
                myList = noteDataSource.getListContract();
                break;

            case 4:
                myList = noteDataSource.getListScheduledDeparture();
                break;

            case 5:
                myList = noteDataSource.getListStoppingPoint();
                break;

            case 6:
                myList = noteDataSource.getListCar();
                break;

            case 7:
                myList = noteDataSource.getListInspection();
                break;

            case 8:
                myList = noteDataSource.getListRepair();
                break;

            default:
                break;
        }

        return myList;
    }


    public void deleteTown(int id) {
        noteDataSource.deleteTown(id);
    }

    public void deleteRoute(int id) {
        noteDataSource.deleteRoute(id);
    }

    public void deleteSheduled(int id) {
        noteDataSource.deleteScheduledDeparture(id);
    }

    public void deletePoint(int id) {
        noteDataSource.deleteStoppingPoint(id);
    }

    public void deleteDriver(int id) {
        noteDataSource.deleteDriver(id);
    }

    public void deleteContract(int id) {
        noteDataSource.deleteContract(id);
    }

    public void deleteCar(int id) {
        noteDataSource.deleteCar(id);
    }

    public void deleteInspect(int id) {
        noteDataSource.deleteInspection(id);
    }

    public void deleteRepair(int id) {
        noteDataSource.deleteRepair(id);
    }

    private void showLog(String show) {
        Log.d(TAG, show);
    }


}
