package transport.frilance.android8.sarad.transportaplication.FragmentAdd;


import android.util.Log;

import transport.frilance.android8.sarad.transportaplication.EntityDataBase.EntityCar;
import transport.frilance.android8.sarad.transportaplication.EntityDataBase.EntityContract;
import transport.frilance.android8.sarad.transportaplication.EntityDataBase.EntityDriver;
import transport.frilance.android8.sarad.transportaplication.EntityDataBase.EntityInspection;
import transport.frilance.android8.sarad.transportaplication.EntityDataBase.EntityRepair;
import transport.frilance.android8.sarad.transportaplication.EntityDataBase.EntityRoute;
import transport.frilance.android8.sarad.transportaplication.EntityDataBase.EntityScheduledDeparture;
import transport.frilance.android8.sarad.transportaplication.EntityDataBase.EntityStoppingPoint;
import transport.frilance.android8.sarad.transportaplication.EntityDataBase.EntityTown;
import transport.frilance.android8.sarad.transportaplication.FunctionalDB.SingletonNoteDataSource;

class ModelFragmentAdd {

    private static final String TAG = "MyTag";

    SingletonNoteDataSource noteDataSource;


    ModelFragmentAdd() {
        noteDataSource = SingletonNoteDataSource.getInstance();
    }

    public void addTown(EntityTown entity) {
        noteDataSource.addTown(entity);
    }

    public void updateTown(EntityTown entity) {
        noteDataSource.updateTown(entity);
    }

    public void updateRoute(EntityRoute entity) {
        noteDataSource.updateRoute(entity);
    }

    public void addRoute(EntityRoute entity) {
        noteDataSource.addRoute(entity);
    }

    public void updateDriver(EntityDriver entity) {
        noteDataSource.updateDriver(entity);
    }

    public void addDriver(EntityDriver entity) {
        noteDataSource.addDriver(entity);
    }


    public void updateContract(EntityContract entity) {
        noteDataSource.updateContract(entity);
    }

    public void addContract(EntityContract entity) {
        noteDataSource.addContract(entity);
    }


    public void updateSheduled(EntityScheduledDeparture entity) {
        noteDataSource.updateScheduledDeparture(entity);
    }

    public void addSheduled(EntityScheduledDeparture entity) {
        noteDataSource.addScheduledDeparture(entity);
    }


    public void updatePointer(EntityStoppingPoint entity) {
        noteDataSource.updateStoppingPoint(entity);
    }


    public void addPointer(EntityStoppingPoint entity) {
        noteDataSource.addStoppingPoint(entity);
    }


    public void updateCar(EntityCar entity) {
        noteDataSource.updateCar(entity);
    }


    public void addCar(EntityCar entity) {
        noteDataSource.addCar(entity);
    }


    public void updateRepair(EntityRepair entity) {
        noteDataSource.updateRepair(entity);
    }


    public void addRepair(EntityRepair entity) {
        noteDataSource.addRepair(entity);
    }


    public void updateInspection(EntityInspection entity) {
        noteDataSource.updateInspection(entity);
    }


    public void addInspection(EntityInspection entity) {
        noteDataSource.addInspection(entity);
    }


    private void showLog(String show) {
        Log.d(TAG, show);
    }

}
