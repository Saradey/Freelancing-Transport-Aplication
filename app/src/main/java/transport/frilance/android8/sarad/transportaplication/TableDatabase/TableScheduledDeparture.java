package transport.frilance.android8.sarad.transportaplication.TableDatabase;


public class TableScheduledDeparture {

    private final String id_scheduled_departure = "id_scheduled_departure";
    private final String date = "date";
    private final String _id_route = "_id_route";
    private final String _id_stopping_point = "_id_stopping_point";
    private final String _id_driver = "_id_driver";
    private final String nameTable = "TableScheduledDeparture";

    public String getId_scheduled_departure() {
        return id_scheduled_departure;
    }

    public String getDate() {
        return date;
    }

    public String get_id_route() {
        return _id_route;
    }

    public String get_id_stopping_point() {
        return _id_stopping_point;
    }

    public String get_id_driver() {
        return _id_driver;
    }

    public String getNameTable() {
        return nameTable;
    }
}
