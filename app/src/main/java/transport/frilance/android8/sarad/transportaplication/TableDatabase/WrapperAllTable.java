package transport.frilance.android8.sarad.transportaplication.TableDatabase;


public class WrapperAllTable {

    private TableCar tableCar = new TableCar();
    private TableContract tableContract = new TableContract();
    private TableDriver tableDriver = new TableDriver();
    private TableInspection tableInspection = new TableInspection();
    private TableRepair tableRepair = new TableRepair();
    private TableRoute tableRoute = new TableRoute();
    private TableScheduledDeparture tableScheduledDeparture = new TableScheduledDeparture();
    private TableStoppingPoint tableStoppingPoint = new TableStoppingPoint();
    private TableTown tableTown = new TableTown();


    public TableCar getTableCar() {
        return tableCar;
    }

    public TableContract getTableContract() {
        return tableContract;
    }

    public TableDriver getTableDriver() {
        return tableDriver;
    }

    public TableInspection getTableInspection() {
        return tableInspection;
    }

    public TableRepair getTableRepair() {
        return tableRepair;
    }

    public TableRoute getTableRoute() {
        return tableRoute;
    }

    public TableScheduledDeparture getTableScheduledDeparture() {
        return tableScheduledDeparture;
    }

    public TableStoppingPoint getTableStoppingPoint() {
        return tableStoppingPoint;
    }

    public TableTown getTableTown() {
        return tableTown;
    }
}
