package transport.frilance.android8.sarad.transportaplication.TableDatabase;


public class TableRoute {

    private final String id_route = "id_route";
    private final String long_route = "long_route";
    private final String _id_town = "_id_town";
    private final String name_route = "name_route";
    private final String nameTable = "Route";

    public String getName_route() {
        return name_route;
    }

    public String getId_route() {
        return id_route;
    }

    public String getLong_route() {
        return long_route;
    }

    public String get_id_town() {
        return _id_town;
    }

    public String getNameTable() {
        return nameTable;
    }
}
