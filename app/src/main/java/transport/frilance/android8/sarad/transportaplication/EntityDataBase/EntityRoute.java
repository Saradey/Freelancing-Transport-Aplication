package transport.frilance.android8.sarad.transportaplication.EntityDataBase;


public class EntityRoute implements Entity {

    private static final String NAME = "Название маршрута: ";
    private static final String ID = "id маршрута: ";
    private static final String LONG = "длина маршрута: ";
    private static final String TOWN = "id города: ";
    private String name_route;
    private int id_route;
    private int long_route;

    private int _id_town;

    public static String getNAME() {
        return NAME;
    }

    public static String getID() {
        return ID;
    }

    public static String getLONG() {
        return LONG;
    }

    public String getName_route() {
        return name_route;
    }

    public void setName_route(String name_route) {
        this.name_route = name_route;
    }

    public int getId_route() {
        return id_route;
    }

    public void setId_route(int id_route) {
        this.id_route = id_route;
    }

    public int getLong_route() {
        return long_route;
    }

    public void setLong_route(int long_route) {
        this.long_route = long_route;
    }

    public int get_id_town() {
        return _id_town;
    }

    public void set_id_town(int _id_town) {
        this._id_town = _id_town;
    }

    @Override
    public int getId() {
        return id_route;
    }

    @Override
    public void setId(int id) {
        id_route = id;
    }


    @Override
    public String toString() {
        String result = ID + id_route + "\n\n" + NAME + name_route + "\n\n" + LONG + long_route
                + "\n\n" + TOWN + _id_town;
        return result;
    }
}
