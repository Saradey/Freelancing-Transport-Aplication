package transport.frilance.android8.sarad.transportaplication.TableDatabase;


public class TableInspection {

    private final String id_inspection = "id_inspection";
    private final String date_inspection = "date_inspection";
    private final String result = "result";
    private final String nameTable = "TableInspection";

    public String getId_inspection() {
        return id_inspection;
    }

    public String getDate_inspection() {
        return date_inspection;
    }

    public String getResult() {
        return result;
    }

    public String getNameTable() {
        return nameTable;
    }
}
