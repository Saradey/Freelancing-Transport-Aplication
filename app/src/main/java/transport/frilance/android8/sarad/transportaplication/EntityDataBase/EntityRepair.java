package transport.frilance.android8.sarad.transportaplication.EntityDataBase;

public class EntityRepair implements Entity {

    private static final String DATE_REPAIT = "Дата ремонта: ";
    private static final String ID = "id ремонта: ";
    private static final String PRICE = "Стоимость ремонта: ";
    private static final String DESCRIPTION = "Описание ремонта: ";
    private String date_repair;
    private int id_repair;
    private double price_repair;
    private String description;

    public static String getDateRepait() {
        return DATE_REPAIT;
    }

    public static String getID() {
        return ID;
    }

    public static String getPRICE() {
        return PRICE;
    }

    public static String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public String getDate_repair() {
        return date_repair;
    }

    public void setDate_repair(String date_repair) {
        this.date_repair = date_repair;
    }

    public int getId_repair() {
        return id_repair;
    }

    public void setId_repair(int id_repair) {
        this.id_repair = id_repair;
    }

    public double getPrice_repair() {
        return price_repair;
    }

    public void setPrice_repair(double price_repair) {
        this.price_repair = price_repair;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int getId() {
        return id_repair;
    }


    @Override
    public void setId(int id) {
        id_repair = id;
    }


    @Override
    public String toString() {
        String result = ID + id_repair + "\n\n" + DATE_REPAIT + date_repair + "\n\n" +
                PRICE + price_repair + "\n\n" + DESCRIPTION + description;
        return result;
    }
}
