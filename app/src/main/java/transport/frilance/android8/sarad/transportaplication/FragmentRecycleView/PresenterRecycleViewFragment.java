package transport.frilance.android8.sarad.transportaplication.FragmentRecycleView;


import java.util.List;

import transport.frilance.android8.sarad.transportaplication.EntityDataBase.Entity;

public class PresenterRecycleViewFragment {

    private intetfaceRecycleViewFragment myView;
    private ModelFragmentRecycleView model;


    PresenterRecycleViewFragment(intetfaceRecycleViewFragment myView) {
        this.myView = myView;
        this.model = new ModelFragmentRecycleView();
    }


    public List<Entity> getList(int index) {
        return model.getList(index);
    }


    public void pressOptionsItemSelected(int doit) {
        myView.goToTheAddFragment(doit);
    }


    public void goUdpate(int doit) {
        myView.goToTheAddFragment(doit);
    }

    public void delete() {
        myView.delete();
    }


    public void deleteTown(int id) {
        myView.deleteEntityRecycle();
        model.deleteTown(id);
    }

    public void deleteRoute(int id) {
        myView.deleteEntityRecycle();
        model.deleteRoute(id);
    }

    public void deleteDriver(int id) {
        myView.deleteEntityRecycle();
        model.deleteDriver(id);
    }

    public void deleteContract(int id) {
        myView.deleteEntityRecycle();
        model.deleteContract(id);
    }

    public void deleteSheduled(int id) {
        myView.deleteEntityRecycle();
        model.deleteSheduled(id);
    }

    public void deletePointer(int id) {
        myView.deleteEntityRecycle();
        model.deletePoint(id);
    }

    public void deleteCar(int id) {
        myView.deleteEntityRecycle();
        model.deleteCar(id);
    }

    public void deleteInspection(int id) {
        myView.deleteEntityRecycle();
        model.deleteInspect(id);
    }

    public void deleteRepair(int id) {
        myView.deleteEntityRecycle();
        model.deleteRepair(id);
    }


}
