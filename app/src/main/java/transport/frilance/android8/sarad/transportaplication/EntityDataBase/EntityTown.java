package transport.frilance.android8.sarad.transportaplication.EntityDataBase;


public class EntityTown implements Entity {

    private static final String ID = "id города: ";
    private static final String NAME = "Название города: ";
    private int id_town;
    private String name_town;

    public static String getNAME() {
        return NAME;
    }

    @Override
    public int getId() {
        return id_town;
    }

    @Override
    public void setId(int id) {
        id_town = id;
    }

    public int getId_town() {
        return id_town;
    }

    public void setId_town(int id_town) {
        this.id_town = id_town;
    }

    public String getName_town() {
        return name_town;
    }

    public void setName_town(String name_town) {
        this.name_town = name_town;
    }

    @Override
    public String toString() {
        String result = ID + id_town + "\n\n" + NAME + name_town;
        return result;
    }
}
